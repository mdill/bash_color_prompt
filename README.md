# Colorize your BASH prompt

## Purpose

This BASH script is designed to give a colorized prompt, based on a number of
criteria:

If the previous command was successful, the prompt will be a `0` with a green
smiling emoticon and green timestamp.  If it was unsuccessful, the `0` will be
the number of failed actions with the rest being red, with a frowny face.

If the user is ROOT, the prompt under this will be red with a blue directory
path.  If the user is not ROOT, the prompt will be green with a blue directory
path, unless there are failed actions, in which case this will resemble the ROOT
prompt.

![User good/bad input](https://bytebucket.org/mdill/bash_color_prompt/raw/3ee5ff63f52fba89d47470135bb70c52851a6228/images/user.png)
![ROOT good/bad input](https://bytebucket.org/mdill/bash_color_prompt/raw/3ee5ff63f52fba89d47470135bb70c52851a6228/images/root.png)

## Download

    cd ~/
    git clone https://bitbucket.org/mdill/bash_color_prompt.git

## Placement

This script is best placed within your `bashrc` (for Linux),
`/etc/bash.bashrc` (for Linux, globally), or `.bash_profile` script, or sourced
within one of those files.

## License

This project is licensed under the BSD License - see the [LICENSE.md](https://bitbucket.org/mdill/bash_color_prompt/src/633dba67cfac79eab1622b72d196444de297454d/LICENSE.txt?at=master) file for
details.

