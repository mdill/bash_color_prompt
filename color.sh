#!/bin/bash

# Determine if we're SSH'd in, or not
host_color='01;32'

if [[ $TMUX != 0 ]]; then
    if [[ -n $SSH_CLIENT ]]; then
        host_color='01;33';
    fi
fi

# Print the PS1 prompt
# Return colorized status with emoticons
PS1="\[\033[01;37m\]\$? \$(if [[ \$? == 0 ]]; then
echo \"\[\033[01;32m\]( ^_^)\";
else echo \"\[\033[01;31m\](._. )\";
fi)

$(if [[ ${EUID} == 0 ]]; then
echo '\[[\t]\n\[\033[01;31m\]\u@\h';
else echo '\[[\t]\n\[\033[01;32m\]\u@\h';
fi)

\[\033[01;34m\] \w \$\[\033[00m\] "

# Tooltip for terminal emulators
if [[ $EUID != 0 && $0 != "-bash" ]]; then
    printf "Here is your terminal emulator...\n\n";
fi

